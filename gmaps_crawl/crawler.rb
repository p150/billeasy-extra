require 'rest-client'
require 'json'
require 'ostruct'
require 'csv'

class Crawler
  API_KEY = 'AIzaSyDlsjuqtrG5s5ce5LFBTUAX_bWT37zgEd0' # Pavan's personal API Key
  API_KEY_2 = 'AIzaSyAytvCyIY5TfC6GYyoobMUJmTeLG41xuEc' # Billeasy Places API Key
  LAT= 19.1630628
  LONG = 72.8762277
  TYPE = 'jewelry_store'
  RADIUS = 2000 # in meters

  def create_csv
    get_ids
    get_details
  end

  def get_ids
    @next_page = @parsed_data.next_page_token if @parsed_data
    @url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=#{LAT},#{LONG}&radius=#{RADIUS}&type=#{TYPE}&key=#{API_KEY}&pagetoken=#{@next_page}"
    response = RestClient.get @url
    @parsed_data = JSON.parse response.body, object_class: OpenStruct
    build_hashed_array
    make_ids_csv
    @next_page = @parsed_data.next_page_token
    if !@next_page.nil?
      get_ids
    end
  end

  def build_hashed_array
    @array_of_ids = []
    @parsed_data.results.each do |data|
      hashy = {
                name: data.name,
                place_id: data.place_id,
                lat: data.geometry.location.lat,
                lng: data.geometry.location.lng
              }
      @array_of_ids << hashy
    end
  end

  def make_ids_csv
    CSV.open("#{TYPE}_#{LAT.to_s + '_' + LONG.to_s}_no_2.csv", 'a+') do |csv|
      @array_of_ids.each do |hash|
        csv << hash.values
      end
    end
  end

  def get_details
    p 'Starting to get details of the places'
    data = []
    hashed_array= []
    CSV.foreach("jewelry_stores_#{LAT.to_s + '_' + LONG.to_s}.csv", { encoding: "UTF-8", headers: false }) do |row|
      data << row
    end
    place_ids = data.map { |e| e[1] }
    place_ids.each_with_index do |id, i|
      url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=#{id}&key=#{API_KEY_2}"
      response = RestClient.get url
      parsed_data = JSON.parse response.body, object_class: OpenStruct
      hashy = {
                name: parsed_data.result.name,
                address: parsed_data.result.formatted_address,
                phone_num: parsed_data.result.formatted_phone_number,
                types: parsed_data.result.types
              }
      p "Detail of Place #{i+1}"
      hashed_array << hashy
    end
    p 'Writing the details to a csv file'
    CSV.open("jewelry_stores_details_#{LAT.to_s + '_' + LONG.to_s}.csv", 'w+') do |csv|
      hashed_array.each do |hash|
        csv << hash.values
      end
    end
    p 'Details are written'
  end
end

crawler = Crawler.new
crawler.create_csv

# ===================================
# accounting
# airport
# amusement_park
# aquarium
# art_gallery
# atm
# bakery
# bank
# bar
# beauty_salon
# bicycle_store
# book_store
# bowling_alley
# bus_station
# cafe
# campground
# car_dealer
# car_rental
# car_repair
# car_wash
# casino
# cemetery
# church
# city_hall
# clothing_store
# convenience_store
# courthouse
# dentist
# department_store
# doctor
# electrician
# electronics_store
# embassy
# establishment (deprecated)
# finance (deprecated)
# fire_station
# florist
# food (deprecated)
# funeral_home
# furniture_store
# gas_station
# general_contractor (deprecated)
# grocery_or_supermarket (deprecated)
# gym
# hair_care
# hardware_store
# health (deprecated)
# hindu_temple
# home_goods_store
# hospital
# insurance_agency
# jewelry_store
# laundry
# lawyer
# library
# liquor_store
# local_government_office
# locksmith
# lodging
# meal_delivery
# meal_takeaway
# mosque
# movie_rental
# movie_theater
# moving_company
# museum
# night_club
# painter
# park
# parking
# pet_store
# pharmacy
# physiotherapist
# place_of_worship (deprecated)
# plumber
# police
# post_office
# real_estate_agency
# restaurant
# roofing_contractor
# rv_park
# school
# shoe_store
# shopping_mall
# spa
# stadium
# storage
# store
# subway_station
# synagogue
# taxi_stand
# train_station
# transit_station
# travel_agency
# university
# veterinary_care
# zoo
# ================================
