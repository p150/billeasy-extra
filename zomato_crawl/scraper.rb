#! /usr/bin/env ruby

require "httparty"
require "nokogiri"
require "pry"
require "csv"

url = "https://www.zomato.com/mumbai/bandra-east-restaurants/lounge?all=1"

headers = {
  "dnt" => "1",
  "accept-encoding" => "gzip, deflate, sdch, br",
  "accept-language" => "en,kn;q=0.8,en-GB;q=0.6,en-US;q=0.4,de-DE;q=0.2,de;q=0.2",
  "upgrade-insecure-requests" => "1",
  "user-agent" => "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36",
  "accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
  "cache-control" => "max-age=0",
  "authority" => "www.zomato.com",
  "cookie" => "PHPSESSID=4110d9f776dc3aee83b8cce17213f45acf1cfce2; fbcity=3; zl=en; fbtrack=bc6646e5229495425ff7eacf964c0d89; dpr=1; lty=zone; ltv=109258; zone=109258; _ga=GA1.2.1555530221.1483612572; __jpuri=https%3A//www.zomato.com/mumbai/goregaon-east-restaurants%3Fall%3D1%26nearby%3D0%26page%3D3"
}

page = HTTParty.get(url, headers: headers)
parse_page = Nokogiri::HTML(page)

rest_names = parse_page.css(".ui.item.menu.search-result-action.mt0").xpath("//a").map { |e| e['data-res-name'] }.compact,
phone_nums = parse_page.css(".ui.item.menu.search-result-action.mt0").xpath("//a").map { |e| e['data-phone-no-str'] }.compact,
address = parse_page.css(".col-m-16.search-result-address.grey-text.nowrap.ln22").map { |e| e['title'] }.compact,
price_for_two = parse_page.css(".res-cost.clearfix").css("span.col-s-11.col-m-12.pl0").map { |e| e.content }.compact
# est_type = parse_page.css(".res-snippet-small-establishment.mt5").search("a").map { |link| link.content }

# binding.pry

data = Array.new(rest_names.map(&:length).max) { |i| rest_names.map{ |e| e[i] } }

File.open("bandra_east_lounges.csv", "a") { |f| f.write(data.inject([]) { |csv, row|  csv << CSV.generate_line(row) }.join("")) }
