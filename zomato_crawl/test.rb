#! /usr/bin/env ruby

require 'rest-client'
require 'json'
require 'ostruct'
require 'csv'

@hashed_array = []
@start = 0

15.times do
  response = RestClient.get "https://developers.zomato.com/api/v2.1/search?entity_id=109258&entity_type=zone&start=#{@start}", { params: {}, 'user-key' => 'b2510b68307bb25cabd07e1dfd148c78' }
  parsed_data = JSON.parse(response.body, object_class: OpenStruct)

  p @start
  p @hashed_array.size
  p parsed_data.results_found

  parsed_data.restaurants.each do |data|
    hashy = {
              id: data.restaurant.id,
              name: data.restaurant.name,
              address: data.restaurant.location.address,
              latitude: data.restaurant.location.latitude,
              longitude: data.restaurant.location.longitude,
              price_for_two: data.restaurant.average_cost_for_two,
              rating: data.restaurant.user_rating.aggregate_rating,
              cuisines: data.restaurant.cuisines
            }
    @hashed_array << hashy
  end

  @start += 20
end

CSV.open('andheri_east.csv', 'w+') do |csv|
  @hashed_array.each do |hash|
    csv << hash.values
  end
end
